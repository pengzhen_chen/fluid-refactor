package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.amq.ActiveMQBroker;
import com.paic.arch.jmsbroker.mq.MQTemplate;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;
import static com.paic.arch.jmsbroker.amq.ActiveMQBroker.BROKER_INSTANCE;
import static org.slf4j.LoggerFactory.getLogger;

public class JmsMessageBrokerSupport {
    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);
    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private static ActiveMQBroker mqBroker;
    private MQTemplate mqTemplate;

    private JmsMessageBrokerSupport(String aBrokerUrl) {
        mqBroker = BROKER_INSTANCE.setBrokerUrl(aBrokerUrl);
        if (mqTemplate == null) {
            mqTemplate = new MQTemplate(mqBroker.getActiveMQConnectionFactory());
        }
    }

    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
    }

    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        JmsMessageBrokerSupport broker = bindToBrokerAtUrl(aBrokerUrl);
        mqBroker.createARunningEmbeddedBrokerAt(aBrokerUrl);
        return broker;
    }

    
    public static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
        return new JmsMessageBrokerSupport(aBrokerUrl);
    }
    
    public void stopTheRunningBroker() throws Exception {
        if (mqBroker.getBrokerService() == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        mqBroker.stopTheRunningBroker();
    }

    public final JmsMessageBrokerSupport andThen() {
        return this;
    }

    public final String getBrokerUrl() {
        return mqBroker.getBrokerUrl();
    }

    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
        mqTemplate.sendATextMessageToDestinationAt(aDestinationName, aMessageToSend);
        return this;
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        return mqTemplate.retrieveASingleMessageFromTheDestination(aDestinationName, aTimeout);
    }
    
    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    }
    
    private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = mqBroker.getBrokerService().getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, mqBroker.getBrokerUrl()));
    }
}
