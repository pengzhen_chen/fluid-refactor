package com.paic.arch.jmsbroker.mq;

/**
 * MQReceiver
 *
 * @author Chenpz
 * @package com.paic.arch.jmsbroker
 * @date 2018/3/22/12:27
 */
public interface MQReceiver {
	/**
	 * 从指定地点接收消息
	 * @param aDestinationName
	 * @return
	 */
	String retrieveASingleMessageFromTheDestination(String aDestinationName);
	
	/**
	 * 从指定地点接收消息 带有 等待时长 限制
	 * @param aDestinationName
	 * @param aTimeout
	 * @return
	 */
	String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout);
}
