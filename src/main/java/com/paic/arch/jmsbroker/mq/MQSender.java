package com.paic.arch.jmsbroker.mq;

/**
 * MQHandler
 *
 * @author Chenpz
 * @package com.paic.arch.jmsbroker
 * @date 2018/3/22/11:26
 */
public interface MQSender{
	/**
	 * 发送消息 到 指定 目的地
	 * @param aDestinationName
	 * @param aMessageToSend
	 */
	void sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend);
}
