package com.paic.arch.jmsbroker.mq;

/**
 * MQTemplate
 *
 * @author Chenpz
 * @package com.paic.arch.jmsbroker
 * @date 2018/3/22/12:32
 */
public interface MQOperations extends MQReceiver, MQSender {
}
