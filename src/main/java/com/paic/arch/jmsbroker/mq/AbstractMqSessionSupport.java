package com.paic.arch.jmsbroker.mq;

import org.slf4j.Logger;

import javax.jms.*;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * AbstractSessionSupport
 *
 * @author Chenpz
 * @package com.paic.arch.jmsbroker.mq
 * @date 2018/3/22/12:06
 */
public abstract class AbstractMqSessionSupport {
	
	private static final Logger LOG = getLogger(AbstractMqSessionSupport.class);
	private ConnectionFactory connectionFactory;
	
	public AbstractMqSessionSupport (ConnectionFactory connectionFactory){
		this.connectionFactory = connectionFactory;
	}
	
	/**
	 * 消息发送
	 * @param aDestinationName
	 * @param aMessageToSend
	 * @return
	 */
	protected Object messageSender(String aDestinationName, final String aMessageToSend) {
		Connection connection = null;
		Session session = null;
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			return sender(session, aDestinationName, aMessageToSend);
		}catch (JMSException e){
			LOG.error("message handle error {}", e);
		}finally {
			close(connection, session);
		}
		return null;
	}
	
	/**
	 * 消息接收
	 * @param aDestinationName
	 * @param aTimeout
	 * @return
	 */
	protected Object messageReceiver(String aDestinationName, int aTimeout){
		Connection connection = null;
		Session session = null;
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			return receiver(session, aDestinationName, aTimeout);
		}catch (JMSException e){
			LOG.error("message handle error {}", e);
		}finally {
			close(connection, session);
		}
		return null;
	
	}
	
	private void close (Connection connection, Session session) {
		try {
			if (session != null) {
				session.close();
			}
			if (connection != null){
				connection.close();
			}
		} catch (JMSException jmse) {
			LOG.warn("Failed to close session or connection {}", session, connection);
		}
	}
	
	/**
	 * 消息发送
	 * @param session
	 * @param aDestinationName
	 * @param aMessageToSend
	 * @return
	 */
	public abstract Object sender (Session session, String aDestinationName, final String aMessageToSend);
	
	/**
	 * 消息接收
	 * @param session
	 * @param aDestinationName
	 * @param aTimeout
	 * @return
	 */
	public abstract Object receiver(Session session, String aDestinationName, int aTimeout);
	

}
