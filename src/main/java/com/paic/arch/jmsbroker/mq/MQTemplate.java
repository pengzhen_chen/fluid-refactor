package com.paic.arch.jmsbroker.mq;

import com.paic.arch.jmsbroker.exception.NoMessageReceivedException;
import org.slf4j.Logger;

import javax.jms.*;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * AMQSupport
 *
 * @author Chenpz
 * @package com.paic.arch.jmsbroker.refactor.factory
 * @date 2018/3/22/12:17
 */
public class MQTemplate extends AbstractMqSessionSupport implements MQOperations {
	
	private static final Logger LOG = getLogger(MQTemplate.class);
	private static final int ONE_SECOND = 1000;
	private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
	
	public MQTemplate (ConnectionFactory connectionFactory) {
		super(connectionFactory);
	}
	
	@Override
	public void sendATextMessageToDestinationAt (String aDestinationName, String aMessageToSend) {
		messageSender(aDestinationName, aMessageToSend);
	}
	
	@Override
	public String retrieveASingleMessageFromTheDestination (String aDestinationName, int aTimeout) {
		return (String)messageReceiver(aDestinationName, aTimeout);
	}
	
	@Override
	public String retrieveASingleMessageFromTheDestination (String aDestinationName) {
		return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
	}
	
	
	@Override
	public Object sender (Session session, String aDestinationName, String aMessageToSend) {
		try {
			Queue queue = session.createQueue(aDestinationName);
			MessageProducer producer = session.createProducer(queue);
			producer.send(session.createTextMessage(aMessageToSend));
			producer.close();
		} catch (JMSException e) {
			LOG.error("sendATextMessageToDestinationAt error:{}", e);
		}
		return "";
	}
	
	@Override
	public Object receiver (Session session, String aDestinationName, int aTimeout) {
		try {
			Queue queue = session.createQueue(aDestinationName);
			MessageConsumer consumer = session.createConsumer(queue);
			Message message = consumer.receive(aTimeout);
			if (message == null) {
				throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
			}
			consumer.close();
			return ((TextMessage) message).getText();
		}catch (JMSException e){
			LOG.error("messages received from the broker error:{}", e);
		}
		return "";
	}
}
