package com.paic.arch.jmsbroker.exception;

/**
 * NoMessageReceivedException
 *
 * @author Chenpz
 * @package com.paic.arch.jmsbroker.exception
 * @date 2018/3/22/14:00
 */
public class NoMessageReceivedException extends RuntimeException {
	public NoMessageReceivedException(String reason) {
		super(reason);
	}
}
