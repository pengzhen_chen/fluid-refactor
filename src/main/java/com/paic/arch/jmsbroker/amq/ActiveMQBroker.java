package com.paic.arch.jmsbroker.amq;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.slf4j.Logger;

import javax.jms.ConnectionFactory;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * ActiveMQBroker
 * ActiveMQ
 * @author Chenpz
 * @package com.paic.arch.jmsbroker.amq
 * @date 2018/3/22/11:11
 */
public class ActiveMQBroker {
	
	private static final Logger LOG = getLogger(ActiveMQBroker.class);
	private String brokerUrl;
	private BrokerService brokerService;
	public static final ActiveMQBroker BROKER_INSTANCE = new ActiveMQBroker();
	
	private ActiveMQBroker(){}
	
	public ConnectionFactory getActiveMQConnectionFactory(){
		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
		factory.setBrokerURL(brokerUrl);
		return factory;
	}
	
	public  ActiveMQBroker createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
		LOG.debug("Creating a new broker at {}", aBrokerUrl);
		ActiveMQBroker broker = bindToBrokerAtUrl(aBrokerUrl);
		broker.createEmbeddedBroker();
		broker.startEmbeddedBroker();
		return broker;
	}
	
	public  ActiveMQBroker bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
		return BROKER_INSTANCE.setBrokerUrl(aBrokerUrl);
	}
	
	private  void createEmbeddedBroker() throws Exception {
		brokerService = new BrokerService();
		brokerService.setPersistent(false);
		brokerService.addConnector(brokerUrl);
		setBrokerUrl(brokerUrl);
	}
	
	public void startEmbeddedBroker() throws Exception {
		brokerService.start();
	}
	
	public void stopTheRunningBroker() throws Exception {
		if (brokerService == null) {
			throw new IllegalStateException("Cannot stop the broker from this API: " +
					"perhaps it was started independently from this utility");
		}
		brokerService.stop();
		brokerService.waitUntilStopped();
	}
	
	public BrokerService getBrokerService(){
		return brokerService;
	}
	
	public String getBrokerUrl(){
		return brokerUrl;
	}
	
	public ActiveMQBroker setBrokerUrl(String brokerUrl){
		this.brokerUrl = brokerUrl;
		return BROKER_INSTANCE;
	}
	
}
